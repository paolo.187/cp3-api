const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const Cart = require('../models/Cart');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Add cart routes here 
module.exports.addToCart = (data) => {

    return Cart.findOne({ "user": data.userId, "isActive" : true }).then(result =>{
         console.log(result)
        if(result){

           
    
           console.log("Cart already exist");
           const item = result.cartItems.find(c => c.product == data.cartItems.product);

           console.log(item)
             //item.product == data.cartItems.product
           if(item){
            console.log("product already added to cart. proceed to add quantity")
            Cart.findOneAndUpdate({user: data.userId, "cartItems.product" : data.cartItems.product, "isActive" : true}, {
                "$set" : {
                    "cartItems.$" : {
                        ...data.cartItems,
                        quantity: item.quantity + data.cartItems.quantity,
                        subtotal: data.price * (item.quantity + data.cartItems.quantity) 
                    }
                }
            })
            .exec((error, _cart)=> {
                if(error) return ({error});
                else if(_cart){
                    console.log("succesfully added quantity")
                    return true;
                } 
            })
            
           }else{
            console.log("Added new item ");
            Cart.findOneAndUpdate({user: data.userId ,"isActive" : true  }, {
                "$push" : {
                    "cartItems" : {
                        product: data.product,
                        name: data.name,
                        quantity: data.quantity,
                        price: data.price,
                        subtotal: data.price * data.quantity
                    }
                }
            })
            .exec((error, _cart)=> {
                if(error) return ({error});
                else if(_cart) return true;
            })
           }
            
        }else{
    
            let cartDetails = new Cart({
    
                user: data.userId,
                cartItems: {
                    product: data.product,
                    name: data.name,
                    quantity: data.quantity,
                    price: data.price,
                    subtotal: data.price * data.quantity
                }
        
            });
    
            return cartDetails.save().then((save,error)=> {
                if(error){
                    console.log("Failed to add item to cart")
                    return false
                }else if(save){
                    console.log("Item has been added to cart")
                    return ({save})
        
                }
            })
        }
        
    })
   
}


// get all cart 
module.exports.allCart = () => {
    return Cart.find({}).then(result =>{
        return result
    })
}

    

// get specific user's cart
module.exports.getCart = (data) => {
    return Cart.findOne({user: data.userId, "isActive" : true}).then(result=> {
        return result
    })
}

    
// checkout 
module.exports.checkout = (data) => {
   return User.findById(data.userId).then(user => {

        return Cart.findById(data.cartId).then(cart => {
            
            const newCartItems = cart.cartItems;
            console.log(newCartItems)

            let orderData = new Order({
            totalAmount: data.totalAmount,
            user: {
                userId: data.userId,
                userEmail: user.email
            }
            
        })

        return orderData.save().then((addOrder, error) => {
            if(error) {
                console.log("failed to save order data")
            }else if(addOrder){
                // insert code here to put cartItems to Order model
               return Order.updateOne({ '_id': orderData.id }, { $addToSet: { "cartItems": { $each: newCartItems } } 

               })
               .exec((error, success)=> {
                if(error){
                 return ({error})
                }
                else if(success){
                    let userOrderData = {

                        orderId: orderData.id,
                        totalAmount: orderData.totalAmount

                    }

                    user.order.push(userOrderData)
                    return user.save().then((userUpdate, error)=> {
                if(userUpdate&&addOrder){
                    console.log("user and order data save")
                    
                    let cartIsAcive = {
                        isActive : false
                    }

                    // make the cart isActive false upon checkout
                    return Cart.findByIdAndUpdate(data.cartId, cartIsAcive).then((cartEdit, err)=> {
                        if(err){
                            return false
                        }else if(cartEdit){
                                return({cartEdit})
                                console.log("successfully update cart isActive to false")
                            
                        }
                    })
                }else{
                    console.log("failed to push data")
                    return false
                }
            })
                } 
            })
        }        
        })
        })
        
    })
}

// remove item from cart
module.exports.removeItem = (data) => {
    return  Cart.findOneAndUpdate({user: data.userId, "isActive" : true}, {
                "$pull" : {
                    "cartItems" : {
                    "_id": data.cartId
                    }
                }
            }) 
}

// add quantity
module.exports.addQuantity = (data) => {
    return Cart.findOne({ user: data.userId, "isActive" : true}).then(result =>{

         const item = result.cartItems.find(c => c.product == data.cartItems.product);
         console.log(item)

         if(item){
            Cart.findOneAndUpdate({user: data.userId, "cartItems.product" : data.cartItems.product, "isActive" : true}, {
                "$set" : {
                    "cartItems.$" : {
                       ...data.cartItems,
                        name: item.name,
                        price: item.price,
                        quantity: item.quantity + 1,
                        subtotal: data.cartItems.price * (item.quantity + 1)


                    }
                }

            })

            .exec((error, _cart)=> {
                if(error) return ({error});
                else if(_cart){
                    console.log("succesfully added quantity")
                  
                    
                } 
            })
         }
     })
}


// minus quantity
module.exports.minusQuantity = (data) => {
    return Cart.findOne({ user: data.userId, "isActive" : true}).then(result =>{

           
         const item = result.cartItems.find(c => c.product == data.cartItems.product);
         console.log(item)

         if(item){
            Cart.findOneAndUpdate({user: data.userId, "cartItems.product" : data.cartItems.product, "isActive" : true}, {
                "$set" : {
                    "cartItems.$" : {
                       ...data.cartItems,
                        name: item.name,
                        quantity: item.quantity - 1,
                        subtotal: data.cartItems.price * (item.quantity - 1)
                    }
                }     

            })
            .exec((error, _cart)=> {
                if(error) return ({error});
                else if(_cart){
                    console.log("succesfully decrease quantity")
                    return({_cart});
                } 
                Cart.findOne({user: data.userId, "cartItems.product" : data.cartItems.product}).then(result =>{

                    console.log(result)
                })

            })
         }
     })
}


// get user order details
module.exports.getUserOrder = (data) => {
    return Order.find({"user.userId": data.userId}).then(result=>{
        return result
    })
}